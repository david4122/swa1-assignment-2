export type Generator<T>= { next:() => T }

export type Position = {
	row: number,
	col: number
}

export type Match<T> = {
	matched: T,
	positions: Position[]
}

export type BoardEvent<T> = {
	kind: 'Match' | 'Refill',
	match?: Match<T>,
};

export type BoardListener<T> = (e: BoardEvent<T>) => void;

export class Board<T> {
	readonly width: number;
	readonly height: number;

	private _listeners: BoardListener<T>[] = [];
	private _gen: Generator<T>;
	private _board: T[][];

	// Constructor here
	constructor(gen: Generator<T>, width: number, height: number) {
		this._gen = gen;
		this.width = width;
		this.height = height;

		this._board = this.buildBoard(gen, width, height);
	}

	addListener(listener: BoardListener<T>) {
		this._listeners.push(listener);
	}

	positions(): Position[] {
		const positions = [];
		for(let i = 0; i < this.height; i++) {
			for(let j = 0; j < this.width; j++) {
				positions.push({ row: i, col: j });
			}
		}

		return positions;
	}

	piece(p: Position): T | undefined {
		return (this.isOnBoard(p)) ? this._board[p.row][p.col] :undefined;
	}

	canMove(p1: Position, p2: Position): boolean {
		if(p1.row !== p2.row && p1.col !== p2.col)
			return false;

		if(!this.isOnBoard(p1) || !this.isOnBoard(p2))
			return false;

		this.swap(p1, p2);
		const matches = this.findMatches(p1);
		matches.push(...this.findMatches(p2));
		this.swap(p2, p1);

		return matches.length > 0;
	}

	move(p1: Position, p2: Position) {
		if(!this.canMove(p1, p2))
			return;

		this.swap(p1, p2);
		const matches = this.findMatches(p1);
		matches.push(...this.findMatches(p2));

		// TODO
	}

	private swap(p1: Position, p2: Position): void {
		const swp = this.piece(p1);
		this._board[p1.row][p1.col] = this._board[p2.row][p2.col];
		this._board[p2.row][p2.col] = swp;
	}

	private findMatches(p: Position): Match<T>[] {
		const matches = [];

		let m = this.findMatchHorizontal(p);
		if(m)
			matches.push(m);

		m = this.findMatchVertical(p);
		if(m)
			matches.push(m);

		return matches;
	}

	private findMatchHorizontal(p: Position): Match<T> | null {
		const matched = this.piece(p);

		let cnt = 0;
		let positions = [];
		const from = Math.max(0, p.col - 2);
		const to = Math.min(this.width, p.col + 3);
		for(let i = from; i < to; i++) {
			const next = { row: p.row, col: i };
			if(this.piece(next) === matched) {
				cnt++;
				positions.push(next);
			} else {
				if(cnt >= 3)
					return { matched, positions };
				cnt = 0;
				positions = [];
			}
		}
		if(cnt >= 3)
			return { matched, positions };

		return null;
	}

	private findMatchVertical(p: Position): Match<T> | null {
		const matched = this.piece(p);

		let cnt = 0;
		let positions = [];
		const from = Math.max(0, p.row - 2);
		const to = Math.min(this.height, p.row + 3);
		for(let i = from; i < to; i++) {
			const next = { row: i, col: p.col };
			if(this.piece(next) === matched) {
				cnt++;
				positions.push(next);
			} else {
				if(cnt >= 3)
					return { matched, positions };
				cnt = 0;
				positions = [];
			}
		}
		if(cnt >= 3)
			return { matched, positions };

		return null;
	}

	private pieceAt = (row: number, col: number) => this._board[row][col];

	private isOnBoard(p: Position): boolean {
		return p.row >= 0 && p.row < this.height && p.col >= 0 && p.col < this.width;
	}

	private buildBoard(gen: Generator<T>, w: number, h: number): T[][] {
		const board = [];

		for(let i = 0; i < h; i++) {
			const row = [...Array(w)].map(i => gen.next());
			board.push(row);
		}

		return board;
	}
}
